provider "aws" {
  region  = "eu-west-1"
  profile = "demo"
}


variable "public_key_value" {
  type        = string
  default     = ""
  description = "public ssh key"
}


variable "private_key_path" {
  type        = string
  default     = ""
  description = "public ssh key"
}


resource "aws_vpc" "app_server_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "test VPC"
  }
}


resource "aws_subnet" "app_server_public_subnet" {
  vpc_id            = aws_vpc.app_server_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "eu-west-1a"

  tags = {
    Name = "Test Public Subnet"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.app_server_vpc.id
  tags = {
    Name = "Test Internet Gateway"
  }
}

resource "aws_route_table" "app_server_route_table" {
  vpc_id = aws_vpc.app_server_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "Test Route Table public subnet"
  }
}

resource "aws_route_table_association" "public_subnet_route_table_ass" {
  subnet_id      = aws_subnet.app_server_public_subnet.id
  route_table_id = aws_route_table.app_server_route_table.id
}


resource "aws_security_group" "allow_ssh" {
  name        = "Allow_ssh_traffic"
  description = "Allow web inbound traffic"
  vpc_id      = aws_vpc.app_server_vpc.id

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["145.18.0.0/16", "146.50.0.0/16", "145.3.0.0/17", "145.109.0.0/17"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "test_allow_ssh"
  }
}


resource "aws_eip" "public_example_eip" {
  vpc      = true
  instance = aws_instance.public_example.id
  tags = {
    Name = "ip address Test Instance Public"
  }


  connection {
    type     = "ssh"
    user     = "admin"
    private_key = file("${var.private_key_path}")
    host     = aws_eip.public_example_eip.public_ip
  }


  provisioner "file" {
    source      = "scripts/script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "/tmp/script.sh",
    ]
  }
}


resource "aws_instance" "public_example" {
  ami                    = "ami-03c5ca432499db74d"
  instance_type          = "t2.micro"
  availability_zone      = "eu-west-1a"
  subnet_id              = aws_subnet.app_server_public_subnet.id
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
  key_name               = "aws_rsa_2"
  tags = {
    Name = "Test Instance Public"
  }
}


resource "aws_key_pair" "deployer" {
  key_name   = "aws_rsa_2"
  public_key = var.public_key_value
}

