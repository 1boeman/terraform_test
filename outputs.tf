output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.public_example.id
}


output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_eip.public_example_eip.public_ip
}
